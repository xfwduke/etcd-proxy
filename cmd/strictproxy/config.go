package strictproxy

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"math"
)

var (
	grpcProxyListenAddr        string
	grpcProxyMetricsListenAddr string
	grpcProxyEndpoints         []string
	grpcProxyDNSCluster        string
	grpcProxyInsecureDiscovery bool
	grpcProxyDataDir           string
	grpcMaxCallSendMsgSize     int
	grpcMaxCallRecvMsgSize     int

	// tls for connecting to etcd

	grpcProxyCA                    string
	grpcProxyCert                  string
	grpcProxyKey                   string
	grpcProxyInsecureSkipTLSVerify bool

	// tls for clients connecting to proxy

	grpcProxyListenCA      string
	grpcProxyListenCert    string
	grpcProxyListenKey     string
	grpcProxyListenAutoTLS bool
	grpcProxyListenCRL     string

	grpcProxyAdvertiseClientURL string
	grpcProxyResolverPrefix     string
	grpcProxyResolverTTL        int

	grpcProxyNamespace string
	grpcProxyLeasing   string

	grpcProxyEnablePprof    bool
	grpcProxyEnableOrdering bool

	grpcProxyDebug bool

	rootCmd = &cobra.Command{
		Use:   "strictproxy",
		Short: "strictproxy",
		Run:   startGRPCProxy,
	}

	grpcProxyReadonly           bool
	grpcProxyPermissionPatterns []string
	cfgFile                     string
)

const defaultGRPCMaxCallSendMsgSize = 1.5 * 1024 * 1024

func init() {
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")

	cobra.OnInitialize(initConfig)

	rootCmd.Flags().String("listen-addr", "127.0.0.1:23790", "listen address")
	rootCmd.Flags().String("discovery-srv", "", "DNS domain used to bootstrap initial cluster")
	rootCmd.Flags().String("metrics-addr", "", "listen for /metrics requests on an additional interface")
	rootCmd.Flags().Bool("insecure-discovery", false, "accept insecure SRV records")
	rootCmd.Flags().StringSlice("endpoints", []string{"127.0.0.1:2379"}, "comma separated etcd cluster endpoints")
	rootCmd.Flags().String("advertise-client-url", "127.0.0.1:23790", "advertise address to register (must be reachable by client)")
	rootCmd.Flags().String("resolver-prefix", "", "prefix to use for registering proxy (must be shared with other grpc-proxy members)")
	rootCmd.Flags().Int("resolver-ttl", 0, "specify TTL, in seconds, when registering proxy endpoints")
	rootCmd.Flags().String("namespace", "", "string to prefix to all keys for namespacing requests")
	rootCmd.Flags().Bool("enable-pprof", false, `Enable runtime profiling data via HTTP server. Address is at client URL + "/debug/pprof/"`)
	rootCmd.Flags().String("data-dir", "default.proxy", "Data directory for persistent data")
	rootCmd.Flags().Int("max-send-bytes", defaultGRPCMaxCallSendMsgSize, "message send limits in bytes (default value is 1.5 MiB)")
	rootCmd.Flags().Int("max-recv-bytes", math.MaxInt32, "message receive limits in bytes (default value is math.MaxInt32)")

	// client TLS for connecting to server
	rootCmd.Flags().String("cert", "", "identify secure connections with etcd servers using this TLS certificate file")
	rootCmd.Flags().String("key", "", "identify secure connections with etcd servers using this TLS key file")
	rootCmd.Flags().String("cacert", "", "verify certificates of TLS-enabled secure etcd servers using this CA bundle")
	rootCmd.Flags().Bool("insecure-skip-tls-verify", false, "skip authentication of etcd server TLS certificates")

	// client TLS for connecting to proxy
	rootCmd.Flags().String("cert-file", "", "identify secure connections to the proxy using this TLS certificate file")
	rootCmd.Flags().String("key-file", "", "identify secure connections to the proxy using this TLS key file")
	rootCmd.Flags().String("trusted-ca-file", "", "verify certificates of TLS-enabled secure proxy using this CA bundle")
	rootCmd.Flags().Bool("auto-tls", false, "proxy TLS using generated certificates")
	rootCmd.Flags().String("client-crl-file", "", "proxy client certificate revocation list file.")

	// experimental flags
	//rootCmd.Flags().Bool("experimental-serializable-ordering", false, "Ensure serializable reads have monotonically increasing store revisions across endpoints.")
	//rootCmd.Flags().String("experimental-leasing-prefix", "", "leasing metadata prefix for disconnected linearized reads.")

	rootCmd.Flags().Bool("debug", false, "Enable debug-level logging for grpc-proxy.")
	rootCmd.Flags().Bool("readonly", true, "Set grpc-proxy to readonly mode.")
	rootCmd.Flags().StringSlice("permission-patterns", []string{"^/"}, "permission patterns")

	viper.BindPFlags(rootCmd.Flags())

}

func initConfig() {
	viper.SetConfigType("yaml")

	if cfgFile != "" {
		fmt.Println(cfgFile)
		viper.SetConfigFile(cfgFile)
		if err := viper.ReadInConfig(); err != nil {
			panic(err)
		}
	}
	fmt.Println("hi")
}

func retriveViper() {
	grpcProxyListenAddr = viper.GetString("listen-addr")
	grpcProxyDNSCluster = viper.GetString("discovery-srv")
	grpcProxyMetricsListenAddr = viper.GetString("metrics-addr")
	grpcProxyEndpoints = viper.GetStringSlice("endpoints")
	grpcProxyAdvertiseClientURL = viper.GetString("advertise-client-url")
	grpcProxyResolverPrefix = viper.GetString("resolver-prefix")
	grpcProxyResolverTTL = viper.GetInt("resolver-ttl")
	grpcProxyNamespace = viper.GetString("namespace")
	grpcProxyEnablePprof = viper.GetBool("enable-pprof")
	grpcProxyDataDir = viper.GetString("data-dir")
	grpcMaxCallSendMsgSize = viper.GetInt("max-send-bytes")
	grpcMaxCallRecvMsgSize = viper.GetInt("max-recv-bytes")
	grpcProxyCert = viper.GetString("cert")
	grpcProxyKey = viper.GetString("key")
	grpcProxyCA = viper.GetString("cacert")
	grpcProxyInsecureSkipTLSVerify = viper.GetBool("insecure-skip-tls-verify")
	grpcProxyListenCert = viper.GetString("cert-file")
	grpcProxyListenKey = viper.GetString("key-file")
	grpcProxyListenCA = viper.GetString("trusted-ca-file")
	grpcProxyListenAutoTLS = viper.GetBool("auto-tls")
	grpcProxyListenCRL = viper.GetString("client-crl-file")
	//grpcProxyEnableOrdering = viper.GetBool("experimental-serializable-ordering")
	//grpcProxyLeasing = viper.GetString("experimental-leasing-prefix")
	grpcProxyDebug = viper.GetBool("debug")
	grpcProxyReadonly = viper.GetBool("readonly")
	grpcProxyPermissionPatterns = viper.GetStringSlice("permission-patterns")

	grpcProxyLeasing = ""
	grpcProxyEnableOrdering = false
}