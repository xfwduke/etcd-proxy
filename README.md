# _key_ 模式受限的 _etcd grpc proxy_

基于原有 _grpcproxy_ 代码修改

## 新增特性

### _key_ 模式限制

`--permission-pattern='^/gcs_prometheus_exporter/{{.RemoteAddr}}'`

1. _{{.RemoteAddr}}_ 为固定的替换模板, 代表客户机的 _IP_ 地址
2. 整个模式为标准的正则表达式
3. 可以有多个模式, 任何一个命中即可以访问
4. 各模式间未做冲突判断

### 只读模式

`--readonly`

默认为 _true_

## 移除的特性

1. _lease proxy_
2. _maintenance proxy_
3. _auth proxy_
4. _election proxy_
5. _lock proxy_
6. _kv proxy_
  * _OpTxn_
  * _OpCompact_


## _example_

```
strict-proxy start \
	--cacert path/to/ca.pem \
	--cert path/to/client.pem \
	--key path/to/client-key.pem \
	--cert-file=path/to/server.pem \
	--key-file=path/to/server-key.pem \
	--permission-pattern='^/gcs_prometheus_exporter/{{.RemoteAddr}}' \
	--permission-pattern='^/otherhead/{{.RemoteAddr}}'
```


```
etcdctl --endpoints=127.0.0.1:23790 --cacert for_ssl/cfssl/ca.pem --cert for_ssl/cfssl/client.pem --key for_ssl/cfssl/client-key.pem get /gcs_prometheus_exporter/127.0.0.2/7000 --prefix
Error: rpc error: code = PermissionDenied desc = access key = /gcs_prometheus_exporter/127.0.0.2/7000, range-end = /gcs_prometheus_exporter/127.0.0.2/7001 denied

```
